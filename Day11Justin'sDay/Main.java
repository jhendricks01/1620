
public class Main {
	
	public static void main(String[]args){
		
		try{
		wakeUp();
		
		getReadyForSchool();
		
		goToClass();
		
		eatLunch();
		
		goToMoreClass();
		
		goHome();
		}
		catch(AbductionException e)
		{
			System.out.println("Justin heard about an abduction on campus. Maybe it was an alien");
			System.out.println("Justin goes back to sleep");
		}
	}

	private static void goHome() {
		// TODO Auto-generated method stub
		System.out.println("Justin takes his hoverboard home. (Don't take it on an airplane)");
	}

	private static void goToMoreClass() {
		// TODO Auto-generated method stub
		System.out.println("Justin tries to stay awake in his later classes");
	}

	private static void eatLunch() {
		// TODO Auto-generated method stub
		System.out.println("Justin eats lunch.  He has a... Double Cheeseburger.");
	}

	private static void goToClass() {
		// TODO Auto-generated method stub
		System.out.println("Justin can't hear anything because of the fan");
	}

	private static void getReadyForSchool() {
		// TODO Auto-generated method stub
		System.out.println("Justin gets ready for school.");
	}

	//option 3: keep throwing the exception
	private static void wakeUp() {
		// TODO Auto-generated method stub
		System.out.println("Justin wakes up!");
		
		//heres how to throw an exception
		
		//option 1: just ignore the exception and let the program terminate
		
		//option 2: catch the exception
		//try{
		throw new AbductionException();
		}
		/*catch(AbductionException e)
		{
			System.out.println("Justin's teach was abuducted by an alien, Hurray!!");
			System.out.println("Justin goes back to sleep");
		}
		
		}*/
	}


